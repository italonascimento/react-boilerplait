import * as React from 'react';

export interface HelloInterface{
  to: string;
}

export function Hello(props: HelloInterface){
  return (
    <p>Hello { props.to }</p>
  )
}
