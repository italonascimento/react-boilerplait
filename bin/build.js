let shell = require('shelljs');

let loading = startLoading('Building');
shell.exec('webpack', () => {
  stopLoading(loading, "\nDone!");
  process.exit();
});

function startLoading(msg){
  let frames = ['.  ', ' . ', '  .', ' . '];
  let i = 0;

  return setInterval(() => {
    process.stdout.clearLine();
    process.stdout.write("\n" + frames[i++] + msg + "\r");
    i &= 3;
  }, 200)
}

function stopLoading(loading, msg){
  clearInterval(loading);
  process.stdout.clearLine();
  if(msg)
  process.stdout.write(msg);
}
