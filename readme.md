# React Boilerplate

A code boilerplate for **React** projects writen in **Typescript**.  
It includes:
* **Webpack** configured with **ts-loader** and **source-map-loader**;
* **axios** for ajax requests.

React and ReactDOM are configured as external dependencies in
`webpack.config.js`.
